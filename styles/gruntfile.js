// Grunfile for SweetUIKit Style
// Copyright (c) 2015-2016 Shota Shimazu
// Licensed under the Apache v2, see LICENSE for detail

module.exports = function(grunt){
    grunt.initConfig({
        sass: {
            dist: {
                files: {
                    "sweetuikit.css": "sweetuikit.scss"
                }
            }
        },
        mincss: {
            compress: {
                files: {
                    "css/sweetuikit.min.css": ["sweetuikit.css"]
                }
            }
        },
        watch : {
            scripts : {
                files : [
                    'scss/sweetuikit.scss'
                ],
                tasks : 'sass mincss'
            }
        }
    });
    // Modules
    grunt.loadNpmTasks('grunt-contrib');
    grunt.loadNpmTasks('grunt-sass');
    grunt.registerTask('default', 'sass mincss');
};
