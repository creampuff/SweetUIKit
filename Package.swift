// Chocolate - A cute, beautiful desktop shell for Creampuff OS.
// This program is mainly written in Swift. (Current ver is 2.2)
// So, this uses Swift Package Manager(SPM).


import PackageDescription
 
let package = Package(
    name: "SweetaUIKit"
    dependencies: [
        .Package(url: "git@github.com:Creampuff/SweetUIKit.git", majorVersion: 1),
        .Package(url: "git@github.com:Creampuff/Overture.git", majorVersion: 1),
        .Package(url: "git@github.com:Creampuff/Crepe.git", majorVersion: 1),
    ],
    testDependencies: [
        .Package(url: "ssh://git@example.com/Tester.git", versions: Version(1,0,0)..<Version(2,0,0)),
    ]
)


#if os(Linux)

#endif

#if os(OSX)

#endif


