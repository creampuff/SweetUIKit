SweetUIKit
-----------
![Version: 0.0.2](https://img.shields.io/badge/Version-0.0.2-pink.svg?style=flat-square)
![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg?style=flat-square)

SweetUIKit is the UI widgets pack for [Chocolate](http://github.com/Creampuff/Chocolate/) and other app.
It provides basic appearance of UI for Web based app and native app using qt. (Qt widgets is scheduled to be addressed in the next version.)

>***WARNING***
>*This is still experimental version now.


# Features 

- Built with web based technologies

- CSS based sweet appearance

- Support Web based app and native app

- Support Qt and Go programming (Be in future)

## How to build it
SweetUIKit is supposed to using the Qt Framework 5.5. 
So, you need to install Qt in your computer.
More, SweetUIKit includes CSS for web based app.
But, original code is written in SCSS. You need to compile these code from SCSS to CSS.


### How to build it
>I'll develop auto build tool.

You can test these experimental styles, compiling its.



## Open Source 
This project is OPEN SOURCE.
So, all source of this project will be open at [Github](http://github.com)
Let's join us and develop it together.
If you want to contribute, you need to create Github account at first and fork this [repository](http://github.com/Creampuff/SweetUIKit/) into your forked repository.
Changed code of your forked repository, please send "pull request" to this repository.

### Copyright and License
Copyright (c) 2014-2015 Shota Shimazu 
This program is freely distributed under the MIT license, see [LICENSE](LICENSE) for detail.

### Contributors 

Shota Shimazu

 

